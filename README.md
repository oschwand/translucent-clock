# Translucent clock

See for pictures https://hackaday.io/project/169638-translucent-clock

## Description

This clock is made of an acrylic frame, a 60 leds ring. The front plate
is translucent, providing a nice glowing effect. The red dot tells the
hours and the green dot tells the minutes.

## Details

The build is not completely finished, everything is still on a
breadboard but a clean PCB is on the way (see last picture). For now,
the time can be set through a serial terminal, but physical buttons will
be on the PCB.

### Hardware

* Arduino pro-micro
* DS3231 clock
* WS2812 rings with 60 leds
* laser-cut acrylic frame

### Software

* Arduino libraries
  * DS1307RTC
  * Time
  * Timezone
  * FastLED
* Custom code for setting time and date through a serial terminal

Run the `prepare.sh` script to download the dependencies and run `make`
and `make upload` to build and upload the firmware to the board.

