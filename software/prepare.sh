git clone https://github.com/sparkfun/Arduino_Boards

mkdir lib; cd lib
git clone https://github.com/PaulStoffregen/DS1307RTC
git clone https://github.com/PaulStoffregen/Time
git clone https://github.com/JChristensen/Timezone
wget https://github.com/FastLED/FastLED/archive/v3.1.3.tar.gz
tar xvf v3.1.3.tar.gz && rm v3.1.3.tar.gz && mv FastLED-3.1.3 FastLED

