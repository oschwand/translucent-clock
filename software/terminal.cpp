#include<Arduino.h>

#include <Timezone.h>
#include <DS1307RTC.h>

extern Timezone tz;
TimeChangeRule *tcr;

#define buffer_size 32
char buffer[buffer_size];
#define answer_size 3*buffer_size
char answer[answer_size];

bool valid_time(unsigned int hours, unsigned int minutes, unsigned int seconds) {
  if (hours >= 24)
    return false;
  if (minutes >= 60)
    return false;
  if (seconds >= 60)
    return false;
  return true;
}

bool valid_date(unsigned int year, unsigned int month, unsigned int day) {
  if (month > 12)
    return false;
  if (day > 31) // How to check if it is valid for the given month ?
    return false;
  return true;
}

void command_time(char* buffer, char* answer) {
    int new_hours   = 0;
    int new_minutes = 0;
    int new_seconds = 0;
    int count = sscanf(buffer, "time %i:%i:%i", &new_hours, &new_minutes, &new_seconds);
    if (count == 3) {
      if (valid_time(new_hours, new_minutes, new_seconds)) {
        snprintf(answer, answer_size, "time: ok %02i:%02i:%02i", new_hours, new_minutes, new_seconds);

        tmElements_t tm;
        time_t utc, local;
        utc   = RTC.get();
        local = tz.toLocal(utc);

        breakTime(local, tm);
        tm.Hour   = new_hours;
        tm.Minute = new_minutes;
        tm.Second = new_seconds;

        local = makeTime(tm);
        utc = tz.toUTC(local);
        breakTime(utc, tm);

        RTC.set(utc);
        setTime(utc);
      }
      else {
        snprintf(answer, answer_size, "time: invalid %i:%i:%i", new_hours, new_minutes, new_seconds);
      }
    }
    else {
      snprintf(answer, answer_size, "syntax error: %s", buffer);
    }
}

/* Expects a UTC date */
/* The local date processing is broken for now */
void command_date(char* buffer, char* answer) {
    unsigned int new_year  = 0;
    unsigned int new_month = 0;
    unsigned int new_day   = 0;
    int count = sscanf(buffer, "date %i-%i-%i", &new_year, &new_month, &new_day);

    if (count == 3) {
      if (valid_date(new_year, new_month, new_day)) {
        snprintf(answer, answer_size, "date: ok %04i-%02i-%02i", new_year, new_month, new_day);

        tmElements_t tm;
        time_t utc, local;
        utc   = RTC.get();
        local = tz.toLocal(utc);

        breakTime(local, tm);
        tm.Year  = new_year - 1970;
        tm.Month = new_month;
        tm.Day   = new_day;

        local = makeTime(tm);
        utc   = tz.toUTC(local);

        RTC.set(utc);
        setTime(utc);
      }
      else {
        snprintf(answer, answer_size, "date: invalid %i-%i-%i", new_year, new_month, new_day);
      }
    }
    else {
      snprintf(answer, answer_size, "syntax error: %s", buffer);
    }
}

// broken with miniterm.py
void terminal() {
  Serial.setTimeout(10000);
  if (Serial.available()) {
    Serial.print("> ");
    
    for(unsigned int i=0; i<buffer_size; i++) {
      buffer[i] = '\0';
    }
    for(unsigned int i=0; i<3*buffer_size; i++) {
      answer[i] = '\0';
    }

    unsigned int size;
    for(size=0; size<buffer_size-1;) {
      int input;
      if (Serial.available()) {
        input = Serial.read();

        switch (input) {
        case '\r':
          Serial.println("");
          buffer[size] = '\0';
          goto parse;
          break;
        case 127: // works at least with screen
          Serial.write('\b'); // non-destructive backspace
          // Serial.write(127);  // non-destructive backspace, not working
          size--;
          break;
        default:
          buffer[size] = input;
          Serial.write(buffer[size]);
          size++;
        }
      }
    }

  parse:
    if (size == 0)
      return;
    if (strncmp(buffer, "time", 4) == 0) {
      command_time(buffer, answer);
      goto answer;
    }
    if (strncmp(buffer, "date", 4) == 0) {
      command_date(buffer, answer);
      goto answer;
    }
    if (strncmp(buffer, "utc", 3) == 0) {
      time_t utc = now();
      snprintf(answer, answer_size, "%04i-%02i-%02iT%02i:%02i:%02iUTC",
               year(utc),
               month(utc),
               day(utc),
               hour(utc),
               minute(utc),
               second(utc)
               );
      goto answer;
    }
    if (strncmp(buffer, "local", 5) == 0) {
      time_t utc   = now();
      time_t local = tz.toLocal(utc, &tcr);
      snprintf(answer, answer_size, "%04i-%02i-%02iT%02i:%02i:%02i%s",
               year(local),
               month(local),
               day(local),
               hour(local),
               minute(local),
               second(local),
               tcr->abbrev
               );
      goto answer;
    }
    snprintf(answer, answer_size, "unknown command: %s", buffer);
    goto answer;

  answer:
    Serial.println(answer);
  }
}

