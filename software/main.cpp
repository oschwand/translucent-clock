#include<Arduino.h>
#include <Wire.h>

#include <Time.h>
#include <Timezone.h>
#include <DS1307RTC.h>
#include <FastLED.h>

#include "terminal.h"

#define SIZE 24
CRGB leds24[24];
CRGB leds60[60];

int first24 = 12;
int first60 = 45;

int pin24 = 6;
int pin60 = 7;

TimeChangeRule CEST = {"CEST", Last, Sun, Mar, 2, 120}; //Central European Summer Time
TimeChangeRule CET  = {"CET ", Last, Sun, Oct, 3, 60};  //Central European Standard Time
Timezone tz(CET, CEST);

time_t local;
char hours, minutes, seconds;

// From https://github.com/JChristensen/Timezone/blob/master/examples/Clock/Clock.pde
// Function to return the compile date and time as a time_t value
time_t compileTime(void) {
#define FUDGE 25        //fudge factor to allow for compile time (seconds, YMMV)

    char *compDate = __DATE__, *compTime = __TIME__, *months = "JanFebMarAprMayJunJulAugSepOctNovDec";
    char chMon[3], *m;
    int d, y;
    tmElements_t tm;
    time_t t;

    strncpy(chMon, compDate, 3);
    chMon[3] = '\0';
    m = strstr(months, chMon);
    tm.Month = ((m - months) / 3 + 1);

    tm.Day = atoi(compDate + 4);
    tm.Year = atoi(compDate + 7) - 1970;
    tm.Hour = atoi(compTime);
    tm.Minute = atoi(compTime + 3);
    tm.Second = atoi(compTime + 6);
    t = makeTime(tm);
    return t + FUDGE;        //add fudge factor to allow for compile time
}

time_t testTime(void) {
    tmElements_t tm;
    time_t t;
    tm.Hour   = 13;
    tm.Minute = 32;
    tm.Second = 17;
    t = makeTime(tm);
    return t;
}

void clock(tmElements_t tm, CRGB* leds, unsigned int size, int first) {
  int led_hours = (size * (tm.Hour % 12)) / 12;
  led_hours += round(((float)tm.Minute / 60) * (size / 12));
  int led_minutes = (size * tm.Minute) / 60;

  leds[(first + led_minutes) % size] = CRGB::Green;
  leds[(first +   led_hours) % size] = CRGB::Red;
}

void decoration24(CRGB* leds) {
  for(unsigned int quarter=0; quarter<4; quarter++) {
    leds[6 * quarter]  = CRGB::White;
    leds[6 * quarter] %= 24;
  }
}

void decoration60(CRGB* leds) {
  for(unsigned int hour=0; hour<12; hour++) {
    leds[5 * hour]  = CRGB::White;
    leds[5 * hour] %= 24;
  }
}
 
void setup() {
  Serial.begin(9600);
  Wire.begin();

  setSyncProvider(RTC.get);
  if(timeStatus()!= timeSet)
    Serial.println("RTC: unable to sync");
  else
    Serial.println("RTC: ok");
  // setTime(tz.toUTC(compileTime()));
  // setTime(tz.toUTC(testTime()));

  FastLED.addLeds<NEOPIXEL, 6>(leds24, 24);
  FastLED.addLeds<NEOPIXEL, 7>(leds60, 60);
  FastLED.setBrightness(10);
}

void loop() {
  terminal();

  local = tz.toLocal(now());
  tmElements_t tm;
  breakTime(local, tm);

  FastLED.clear();

  decoration24(leds24);
  decoration60(leds60);

  clock(tm, leds24, 24, first24);
  clock(tm, leds60, 60, first60);

  FastLED.show();

  delay(1000);
}
